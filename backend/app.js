const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors()); // Enable CORS for all origins

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

app.post('/confectionary', async (req, res) => {
    try {
        const { id, name } = req.body;
        const result = await submitTransaction('CreateCake', id, name);
        res.status(204).send("uploaded successfully");
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.get('/confectionary/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadCake', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

// Dynamic route for approving oven, flour, and shop
app.put('/confectionary/:approvalType/:id', async (req, res) => {
    try {
        const { approvalType, id } = req.params;
        const { org } = req.body; // Only necessary for shop approval

        let result;
        if (approvalType === 'approveOven' || approvalType === 'approveFlour') {
            result = await submitTransaction(approvalType, id);
        } else if (approvalType === 'approveShop' && (org === 'shop1' || org === 'shop2')) {
            result = await submitTransaction(approvalType, id, org);
        } else {
            return res.status(400).send('Invalid approval type or organization');
        }

        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@confectionary.com');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));

    const connectionOptions = {
        wallet, identity: identity, discovery: { enabled: false, asLocalhost: true }
    };

    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('confectionarychannel');
    const contract = network.getContract('supplychainmgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    console.log("result", result.toString());
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, World!');
});


module.exports = app; // Exporting app