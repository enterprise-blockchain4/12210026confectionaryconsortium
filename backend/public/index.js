$(document).ready(function () {
    // Handle create cake form submission
    $('#createCakeForm').submit(function (e) {
        e.preventDefault();
        const id = $('#cakeId').val();
        const name = $('#cakeName').val();

        $.ajax({
            url: 'http://localhost:3000/confectionary',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ id, name }),
            success: function () {
                $('#successModal').modal('show');
            },
            error: function (xhr) {
                $('#message').text(`Error: ${xhr.responseText}`);
            }
        });
    });

    // Handle get cake form submission
    $('#getCakeForm').submit(function (e) {
        e.preventDefault();
        const id = $('#searchCakeId').val();
        console.log("name", id);

        $.ajax({
            url: `http://localhost:3000/confectionary/${id}`,
            method: 'GET',
            success: function (data) {
                const cake = JSON.parse(data);
                console.log("cake", cake);

                $('#cakeIdLabel').text(`ID : ${cake.id}`);
                $('#cakeNameLabel').text(`Name : ${cake.name}`); // Display cake name
                $('#approveOven').prop('checked', cake.oven);
                $('#approveFlour').prop('checked', cake.flour);
                $('#approveShop').val(cake.shop ? cake.shop : '');
                $('#manageCakeForm').removeClass('d-none');
            },
            error: function (xhr) {
                $('#message').text(`Error: ${xhr.responseText}`);
            }
        });
    });

    // Handle approve oven button click
    $('#approveOvenButton').click(function () {
        const id = $('#searchCakeId').val();

        $.ajax({
            url: `http://localhost:3000/confectionary/approveOven/${id}`,
            method: 'PUT',
            success: function () {
                $('#message').text('Oven approved successfully');
                $('#approveOven').prop('checked', true);
            },
            error: function (xhr) {
                $('#message').text(`Error: ${xhr.responseText}`);
            }
        });
    });

    // Handle approve flour button click
    $('#approveFlourButton').click(function () {
        const id = $('#searchCakeId').val();

        $.ajax({
            url: `http://localhost:3000/confectionary/approveFlour/${id}`,
            method: 'PUT',
            success: function () {
                $('#message').text('Flour approved successfully');
                $('#approveFlour').prop('checked', true);
            },
            error: function (xhr) {
                $('#message').text(`Error: ${xhr.responseText}`);
            }
        });
    });

    // Handle approve shop button click
    $('#approveShopButton').click(function () {
        const id = $('#searchCakeId').val();
        const org = $('#approveShop').val();

        $.ajax({
            url: `http://localhost:3000/confectionary/approveShop/${id}`,
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({ org }),
            success: function () {
                $('#message').text('Shop approved successfully');
                $('#approveShop').val(org);
            },
            error: function (xhr) {
                $('#message').text(`Error: ${xhr.responseText}`);
            }
        });
    });
});
