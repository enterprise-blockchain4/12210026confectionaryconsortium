package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Cake struct {
	ID     string ` json:"id"`
	Name   string ` json:"name"`
	Oven   bool   ` json:"oven"`
	Flour  bool   ` json:"flour"`
	Shop   bool   ` json:"shop"`
}

type CakeContract struct {
	contractapi.Contract
}

func (c *CakeContract) ApproveOven(ctx contractapi.TransactionContextInterface, id string) error {
	cake, err := c.ReadCake(ctx, id)
	if err != nil {
		return err
	}
	cake.Oven = true
	return c.UpdateCake(ctx, cake)
}

func (c *CakeContract) ApproveFlour(ctx contractapi.TransactionContextInterface, id string) error {
	cake, err := c.ReadCake(ctx, id)
	if err != nil {
		return err
	}
	cake.Flour = true
	return c.UpdateCake(ctx, cake)
}

func (c *CakeContract) ApproveShop(ctx contractapi.TransactionContextInterface, id string, shop string) error {
	if shop != "shop1" && shop != "shop2" {
		return fmt.Errorf("shop can only be approved by shop1 or shop2")
	}
	cake, err := c.ReadCake(ctx, id)
	if err != nil {
		return err
	}
	cake.Shop = true
	return c.UpdateCake(ctx, cake)
}

func (c *CakeContract) CreateCake(ctx contractapi.TransactionContextInterface, id string, name string) error {
	cake := Cake{
		ID:   id,
		Name: name,
	}
	cakeJSON, err := json.Marshal(cake)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, cakeJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	return nil
}

func (c *CakeContract) ReadCake(ctx contractapi.TransactionContextInterface, id string) (*Cake, error) {
	cakeJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if cakeJSON == nil {
		return nil, fmt.Errorf("the cake %s does not exist", id)
	}
	var cake Cake
	err = json.Unmarshal(cakeJSON, &cake)
	if err != nil {
		return nil, err
	}
	return &cake, nil
}

func (c *CakeContract) UpdateCake(ctx contractapi.TransactionContextInterface, cake *Cake) error {
	cakeJSON, err := json.Marshal(cake)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(cake.ID, cakeJSON)
	if err != nil {
		return fmt.Errorf("failed to update world state: %v", err)
	}
	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(CakeContract))
	if err != nil {
		fmt.Printf("Error creating cake chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting cake chaincode: %s", err.Error())
	}
}