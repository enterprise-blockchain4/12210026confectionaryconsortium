
#   CAPABILITIES
Capabilities:
  Application: &ApplicationCapabilities
    V2_0: true
  Orderer: &OrdererCapabilities
    V2_0: true
  Channel: &ChannelCapabilities
    V2_0: true

# ORGANIZATIONS
Organizations:
  - &Orderer
    Name: Orderer
    ID: OrdererMSP
    MSPDir: ./crypto-config/ordererOrganizations/confectionary.com/msp
    Policies: &OrdererPolicies
      Readers:
          Type: Signature
          Rule: "OR('OrdererMSP.member')"
      Writers:
          Type: Signature
          Rule: "OR('OrdererMSP.member')"
      Admins:
          Type: Signature
          # ONLY Admin Role can carry out administration activities
          Rule: "OR('OrdererMSP.admin')"
      Endorsement:
          Type: Signature
          Rule: "OR('OrdererMSP.member')"

  - &Confectionary
    Name: Confectionary
    ID: ConfectionaryMSP
    MSPDir: ./crypto-config/peerOrganizations/confectionary.com/msp
    Policies: &ConfectionaryPolicies
      Readers:
          Type: Signature
          # Any member can READ e.g., query
          Rule: "OR('ConfectionaryMSP.member')"
      Writers:
          Type: Signature
          # Any member can WRITE e.g., submit transaction
          Rule: "OR('ConfectionaryMSP.member')"
      Admins:
          Type: Signature
          # Either Confectionary admin OR Orderer Admin can carry out admin activities
          Rule: "OR('ConfectionaryMSP.admin')"
      Endorsement:
          Type: Signature
          # Any member can act as an endorser
          Rule: "OR('ConfectionaryMSP.member')"
    AnchorPeers:
      - Host: shop1.confectionary.com
        Port: 7051
      - Host: shop2.confectionary.com
        Port: 7051

  - &Flourmill
    Name: Flourmill
    ID: FlourmillMSP
    MSPDir: ./crypto-config/peerOrganizations/flourmill.com/msp
    Policies: &FlourmillPolicies
      Readers:
          Type: Signature
          # Any member
          Rule: "OR('FlourmillMSP.member')"
      Writers:
          Type: Signature
          # Any member
          Rule: "OR('FlourmillMSP.member')"
      Admins:
          Type: Signature
          # BOTH Flourmill Admin AND Orderer Admin needed for admin activities
          Rule: "OR('FlourmillMSP.member')"
      Endorsement:
          Type: Signature
          Rule: "OR('FlourmillMSP.member')"
    AnchorPeers:
      - Host: mill.flourmill.com
        Port: 7051

  - &Bakingstuff
    Name: Bakingstuff
    ID: BakingstuffMSP
    MSPDir: ./crypto-config/peerOrganizations/bakingstuff.com/msp
    Policies: &BakingstuffPolicies
      Readers:
          Type: Signature
          # Any member
          Rule: "OR('BakingstuffMSP.member')"
      Writers:
          Type: Signature
          # Any member
          Rule: "OR('BakingstuffMSP.member')"
      Admins:
          Type: Signature
          # BOTH Bakingstuff Admin AND Orderer Admin needed for admin activities
          Rule: "OR('BakingstuffMSP.member')"
      Endorsement:
          Type: Signature
          Rule: "OR('BakingstuffMSP.member')"
    AnchorPeers:
      - Host: ovens.bakingstuff.com
        Port: 7051

#   ORDERER
Orderer: &OrdererDefaults

  OrdererType: solo

  Addresses:
    # UPDATE THE IP ADDRESS
    - orderer.confectionary.com:7050
  # Policies for Orderer
  Policies:
    Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
    Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
    Admins:
        Type: ImplicitMeta  
        Rule: "ANY Admins"

    # BlockValidation specifies what signatures must be included in the block
    # from the orderer for the peer to validate it.
    BlockValidation:
        Type: ImplicitMeta
        Rule: "ANY Writers"

  # Batch Timeout: The amount of time to wait before creating a batch
  BatchTimeout: 2s

  # Batch Size: Controls the number of messages batched into a block
  BatchSize:
    MaxMessageCount: 10
    AbsoluteMaxBytes: 98 MB
    PreferredMaxBytes: 512 KB

  Capabilities:
    <<: *OrdererCapabilities

Application: &ApplicationDefaults

  ACLs: &ACLsDefault
    
    # ACL policy for lscc's "getid" function
    lscc/ChaincodeExists: /Channel/Application/Readers

    # ACL policy for lscc's "getdepspec" function
    lscc/GetDeploymentSpec: /Channel/Application/Readers

    # ACL policy for lscc's "getccdata" function
    lscc/GetChaincodeData: /Channel/Application/Readers

    # ACL Policy for lscc's "getchaincodes" function
    lscc/GetInstantiatedChaincodes: /Channel/Application/Readers
    

    #---Query System Chaincode (qscc) function to policy mapping for access control---#

    # ACL policy for qscc's "GetChainInfo" function
    qscc/GetChainInfo: /Channel/Application/Readers
    

    # ACL policy for qscc's "GetBlockByNumber" function
    qscc/GetBlockByNumber: /Channel/Application/Readers

    # ACL policy for qscc's  "GetBlockByHash" function
    qscc/GetBlockByHash: /Channel/Application/Readers

    # ACL policy for qscc's "GetTransactionByID" function
    qscc/GetTransactionByID: /Channel/Application/Readers

    # ACL policy for qscc's "GetBlockByTxID" function
    qscc/GetBlockByTxID: /Channel/Application/Readers

    #---Configuration System Chaincode (cscc) function to policy mapping for access control---#

    # ACL policy for cscc's "GetConfigBlock" function
    cscc/GetConfigBlock: /Channel/Application/Readers

    # ACL policy for cscc's "GetConfigTree" function
    cscc/GetConfigTree: /Channel/Application/Readers

    # ACL policy for cscc's "SimulateConfigTreeUpdate" function
    cscc/SimulateConfigTreeUpdate: /Channel/Application/Readers

    #---Miscellanesous peer function to policy mapping for access control---#

    # ACL policy for invoking chaincodes on peer
    peer/Propose: /Channel/Application/Writers

    # ACL policy for chaincode to chaincode invocation
    peer/ChaincodeToChaincode: /Channel/Application/Readers

    #---Events resource to policy mapping for access control###---#

    # ACL policy for sending block events
    event/Block: /Channel/Application/Readers

    # ACL policy for sending filtered block events
    event/FilteredBlock: /Channel/Application/Readers

    # Chaincode Lifecycle Policies introduced in Fabric 2.x
    # ACL policy for _lifecycle's "CheckCommitReadiness" function
    _lifecycle/CheckCommitReadiness: /Channel/Application/Writers

    # ACL policy for _lifecycle's "CommitChaincodeDefinition" function
    _lifecycle/CommitChaincodeDefinition: /Channel/Application/Writers

    # ACL policy for _lifecycle's "QueryChaincodeDefinition" function
    _lifecycle/QueryChaincodeDefinition: /Channel/Application/Readers
    

  # Default policies
  Policies: &ApplicationDefaultPolicies

    # --channel-config-policy
    Endorsement:
        Type: ImplicitMeta
        Rule: "ANY Endorsement"
    Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
    Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
    Admins:
        Type: ImplicitMeta
        Rule: "ANY Admins"
    LifecycleEndorsement:
        Type: ImplicitMeta
        Rule: "ANY Endorsement"

  Organizations:

  Capabilities:
    <<: *ApplicationCapabilities

#   CHANNEL
Channel: &ChannelDefaults
  Policies:
    Readers:
        Type: ImplicitMeta
        Rule: "ANY Readers"
    Writers:
        Type: ImplicitMeta
        Rule: "ANY Writers"
    Admins:
        Type: ImplicitMeta
        Rule: "ANY Admins"
  Capabilities:
    <<: *ChannelCapabilities
 
#   PROFILES
Profiles:
  ConfectionaryOrdererGenesis:
    <<: *ChannelDefaults
    Orderer:
        <<: *OrdererDefaults
        Organizations:
            - <<: *Orderer
    Consortiums:
      ConfectionaryConsortium:
          Organizations:
            - <<: *Confectionary
            - <<: *Flourmill
            - <<: *Bakingstuff
    Application:
        <<: *ApplicationDefaults
        Organizations:
            - <<: *Confectionary
            - <<: *Flourmill
            - <<: *Bakingstuff
  ConfectionaryChannel:
    <<: *ChannelDefaults
    Consortium: ConfectionaryConsortium
    Application:
        <<: *ApplicationDefaults
        Organizations:
            - <<: *Confectionary
            - <<: *Flourmill
            - <<: *Bakingstuff